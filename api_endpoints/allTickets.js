const { Pnr } = require("../database/model/p_index");

exports.allTickets = function (req, res) {
  Pnr.find({}, (err, result) => {
    if (err) {
      return res.status(500);
    }
    res.status(200).send({ data: result, message: "Success" });
  });
};
