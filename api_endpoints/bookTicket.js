const { Pnr } = require("../database/model/p_index");
exports.bookTicket = function (req, res) {
  const { pnrNo, passName, age, trainNo, fromStation,toStation,confirm } = req.body;
  let pnr = new Pnr({
    pnrNo: pnrNo,
    passName: passName,
    age: age,
    trainNo: trainNo,
    fromStation:fromStation,
    toStation:toStation,
    confirm: confirm,
  });
  pnr.save(function (err,data) {
    if (err) {
      return res.status(500);
    }
    res.status(201).send({ "data": pnr, "message": "Booked Successfully" });
  });
};
