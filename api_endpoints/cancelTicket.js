const { Pnr } = require("../database/model/p_index");

exports.cancelTicket = function (req, res) {
    const {passName} = req.body;
        Pnr.findOneAndRemove({passName:passName}, function(err, doc) {
            if(err) return res.status(500);
            if(doc){
                res.status(204).send({"message":"Ticket cancelled"});
            } else {
                res.status(404).send({"message":"Ticket Not Found"});
            }
        })
  };
  