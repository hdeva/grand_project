
const { Contact } = require("../database/model/c_index");
exports.contactAdd = function (req, res) {
  const { customerCareNumbers, Language, itickets, forCancellationEtickets, iMudra,SBICardHelpline } = req.body;
  let contact = new Contact({
    customerCareNumbers: customerCareNumbers,
    Language: Language,
    itickets: itickets,
    forCancellationEtickets: forCancellationEtickets,
    iMudra:iMudra,
    SBICardHelpline:SBICardHelpline,
    
  });
  contact.save(function (err,data) {
    if (err) {
      return res.status(500);
    }
    res.status(201).send({ "data": contact, "message": "Contact added Successfully" });
  });
};

exports.contact = function (req, res) {
  Contact.find({}, (err, result) => {
    if (err) {
      return res.status(500);
    }
    res.status(200).send({ data: result, message: "Success" });
  });
};




