//Login
// importing user model from models
const { User } = require("../database/model/u_index");

/**
 * function returns login confirmation,
 * if username or password are not valid it throws error
 * @param {*} req request object
 * @param {*} res response object
 */
exports.user_login = function (req, res) {
  // request is sent through body-parser in json format
  const { Username, Password } = req.body;
  //finds a username and validates it with it's password
  User.findOne({ Username: Username }, function (err, data) {
    if (err) {
      return res.status(500);
    }
    if (data == null) {
      res
        .status(404)
        .send({
          Username: "invalid",
          Password: "false",
          status: "inactive",
          message: "Username not found",
        });
    } else {
      User.findOne(
        ({ Username: Username }, { Password: Password }),
        function (err, data2) {
          if (err) {
            return res.status(500);
          }
          if (data2 == null) {
            res
              .status(401)
              .send({
                Username: Username,
                password: "false",
                status: "inactive",
                message: "wrong password",
              });
          } else {
            res
              .status(200)
              .send({
                userId: data2._id,
                name: data2.Name,
                Username: Username,
                Password: "true",
                status: "active",
                loginTime: new Date(),
                message: "Login successful",
              });
          }
        }
      );
    }
  });
};
