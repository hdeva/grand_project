const { Pnr } = require("../database/model/p_index");

exports.pnrStatus = function (req, res) {
    const {pnrNo}=req.body
    Pnr.find({pnrNo:pnrNo}, (err, result) => {
      if (err) {
        return res.status(500);
      }
      res.status(200).send({ data: result, message: "Success" });
    });
  };