//Registration
// importing user model from models
const { User } = require("../database/model/u_index");

/**
 * function takes input for new user,
 * if user already exits, throws error
 *
 * @param {*} req request object
 * @param {*} res response object
 */
exports.user_register = function (req, res) {
  // request is sent through body parser in json format
  const { Name, Username, Password, Nationality } = req.body;
  //finds a username
  User.findOne({ Username: Username }, function (err, data) {
    if (err) {
      //intenal server error
      return res.status(500);
    }
    
    if (data == null) {
      // creating a new user
      let u = new User({
        Name: Name,
        Username: Username,
        Password: Password,
        Nationality: Nationality,
      });
      u.save(function (err) {
        if (err) {
          return res.status(500);
        }
        res.status(201).send({ data: u, message: "Registered Successfully" });
      });
    } else {
      res.status(226).send({ message: "User already exist" });
    }
  });
};
