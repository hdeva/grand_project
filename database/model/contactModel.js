const mongoose=require("mongoose")
const { contactSchema }=require("../schema/contactIndex")

const Contact= mongoose.model("contact", contactSchema)

module.exports={Contact}