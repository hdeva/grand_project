const mongoose = require("mongoose");
const { Schema } = mongoose;

const contactSchema = new Schema({
  customerCareNumbers: { type: String, required: true },
  Language: { type: String, required: true },
  itickets: { type: String, required: true },
  forCancellationEtickets: { type: String, required: true },
  iMudra: { type: String, required: true },
  SBICardHelpline: { type: String, required: true },
});

module.exports = { contactSchema };

/* "Customer Care Numbers": "14646 OR 0755-6610661 / 0755-4090600",
    "Language": "Hindi and English",
    "I-tickets/e-tickets": "care@irctc.co.in",
    "For Cancellation E-tickets": "etickets@irctc.co.in",
    "For IRCTC iMudra Prepaid Wallet & Card": "imudracare@irctc.co.in", 
    Railway SBI Card Helpline nos. at 0124-39021212 or 18001801295*/
