const mongoose = require("mongoose");
const { Schema } = mongoose;

const pnrSchema = new Schema({
  pnrNo: { type: String, required: true },
  passName: { type: String, required: true, index: { unique: true } },
  age: { type: String, required: true },
  fromStation:{ type: String, required: true },
  toStation:{ type: String, required: true },
  trainNo: { type: String, required: true },
  confirm: { type: String, required: true }
});

module.exports = { pnrSchema };
