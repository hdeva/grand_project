const mongoose = require("mongoose");
const { Schema } = mongoose;

const userSchema = new Schema({
  Name: { type: String, required: true },
  Username: { type: String, required: true, index: { unique: true } },
  Password: { type: String, required: true },
  Nationality: { type: String, required: true },
});

module.exports = { userSchema };
