// external dependencies
const express = require("express");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
const bodyParser = require("body-parser");
// creating and defining an Express application
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// internal dependencies

const { PORT, MONGO_URL } = require("./constants");
const User = require("./routes/userRoute");
const Contactus = require("./routes/contactRoute");
const bookTicket = require("./routes/ticketRoute");
const cancelTicket = require("./routes/ticketRoute");
const allTickets = require("./routes/ticketRoute");
const pnrStatus = require("./routes/ticketRoute");
//api endpoints rendering
app.use("/", User);
app.use("/", Contactus);
app.use("/ticket", bookTicket);
app.use("/ticket", cancelTicket);
app.use("/ticket", allTickets);
app.use("/ticket", pnrStatus);

// connecting to a MongoDB instance
mongoose.connect(MONGO_URL, () => {
  // on success of connectionl try listening on PORT
  app.listen(PORT, () => {
    console.log(
      "Database connection is Ready and " + "Server is Listening on Port ",
      PORT
    );
  });
});
