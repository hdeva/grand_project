const express = require('express');
const router = express.Router();
const contact = require("../api_endpoints/contact");



router.post('/contact', contact.contact);

module.exports = router;

