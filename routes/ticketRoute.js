const express = require('express');
const router = express.Router();
const bookTicket = require("../api_endpoints/bookTicket");
const cancelTicket=require("../api_endpoints/cancelTicket");
const allTickets=require("../api_endpoints/allTickets");
const pnrStatus=require("../api_endpoints/pnrStatus");

router.post('/cancelticket', cancelTicket.cancelTicket);
router.post('/bookticket', bookTicket.bookTicket);
router.post('/alltickets', allTickets.allTickets);
router.post('/pnrStatus', pnrStatus.pnrStatus);

module.exports = router;