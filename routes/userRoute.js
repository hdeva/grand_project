const express = require('express');
const router = express.Router();
const login = require("../api_endpoints/login");
const register = require("../api_endpoints/register");


router.post('/register', register.user_register);
router.post('/login', login.user_login);
module.exports = router;